FROM elasticsearch:6.8.4

RUN bin/elasticsearch-plugin install analysis-phonetic && bin/elasticsearch-plugin install analysis-icu
